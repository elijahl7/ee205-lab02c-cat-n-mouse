///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///
/// @author  Elijah Lopez <elijahl7@hawaii.edu>
/// @date    22_Jan_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#define DEFAULT_MAX_NUMBER 2048

int main(int argc, char *argv[])
{
   int theMaxValue;
   if (argc > 1)
   {
      if (atoi(argv[1]) >= 1)
      {
         theMaxValue = atoi(argv[1]);
      }
      else
      {
         fprintf(stderr, "Integer value \"%s\" is invalid\n", argv[1]);
         return 1;
      }
   }
   else
   {
      theMaxValue = DEFAULT_MAX_NUMBER;
   }

   int theNumberImThinkingOf = (rand() % theMaxValue) + 1;

   int aGuess;
   do
   {
      printf("OK cat, I\'m thinking of a number from 1 to %d. Make a guess: ", theMaxValue);
      scanf("%d", &aGuess);

      if (aGuess < 1)
      {
         printf("You must enter a number that\'s >= 1\n");
         continue;
      }
      else if (aGuess > theMaxValue)
      {
         printf("You must enter a number that\'s <= %d\n", theMaxValue);
         continue;
      }

      if (aGuess > theNumberImThinkingOf)
      {
         printf("No cat... the number I\'m thinking of is smaller than %d\n", aGuess);
         continue;
      }
      else if (aGuess < theNumberImThinkingOf)
      {
         printf("No cat... the number I\'m thinking of is larger than %d\n", aGuess);
         continue;
      }

   } while (aGuess != theNumberImThinkingOf);

   printf("You got me.\n");
   printf("|\\---/|\n");
   printf("| o_o |\n");
   printf(" \\_^_/\n");

   return 0;
}
